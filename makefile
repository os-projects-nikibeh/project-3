all: mainWorker.out

mainWorker.out: mainWorker.o 
		g++ -std=c++11 -pthread mainWorker.o -o mainWorker.out

mainWorker.o: mainWorker.cpp mainWorker.hpp 
		g++ -std=c++11 -c mainWorker.cpp

clean:
		rm *.o
		rm *.out
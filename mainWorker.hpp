#ifndef MAINWORKER_HPP
#define MAINWORKER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <thread>
#include <chrono>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>

using namespace std;

struct monitorsReturnType{
	chrono::milliseconds entranceTime;
	chrono::milliseconds exitTime;
	int emission;
};

class monitor
{
public:
	monitor();
	~monitor();
    monitorsReturnType carsPathPartEmissionCalc(int carP, int pathPartEmission, int& totalEmission);
    void totalEmissionCalc(int& totalEmission, int carsPathPartEmission);

private:
    sem_t edgeSem;
};

void carWorker(map<string, int> pathPartsDifficultyTable, string carsPath, 
				int carP, int& totalEmission, int pathNumber, int carNumber, vector<string> edgesInOrder);

#endif
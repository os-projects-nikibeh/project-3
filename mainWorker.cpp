#include "mainWorker.hpp"


vector<monitor*> allMonitors;
monitor totalEmissionMonitor;

monitor::monitor()
{
    sem_init(&edgeSem, 0, 1);
}

monitor::~monitor()
{
    sem_destroy(&edgeSem);
}

monitorsReturnType monitor::carsPathPartEmissionCalc(int carP, int pathPartEmission, int& totalEmission)
{
    int carsPathPartEmission = 0;
    sem_wait(&edgeSem);
    chrono::milliseconds entranceT = chrono::duration_cast< chrono::milliseconds> (
        chrono::system_clock::now().time_since_epoch()
    );
    for(int k = 0; k < 10000000 + 1; k++)
        carsPathPartEmission += floor((double)k / ((double)1000000 * carP * pathPartEmission));
    totalEmissionMonitor.totalEmissionCalc(totalEmission, carsPathPartEmission);
    chrono::milliseconds exitT = chrono::duration_cast< chrono::milliseconds> (
        chrono::system_clock::now().time_since_epoch()
    );
    sem_post(&edgeSem);
    monitorsReturnType carInfo;
    carInfo.entranceTime = entranceT;
    carInfo.exitTime = exitT;
    carInfo.emission = carsPathPartEmission;
    return carInfo;
}

void monitor::totalEmissionCalc(int& totalEmission, int carsPathPartEmission)
{
    sem_wait(&edgeSem);
    totalEmission += carsPathPartEmission;
    sem_post(&edgeSem);
}

void carWorker(map<string, int> pathPartsDifficultyTable, string carsPath,
                int carP, int& totalEmission, int pathNumber, int carNumber, vector<string> edgesInOrder)
{
    string outputFileName = to_string(pathNumber) + "-" + to_string(carNumber);
    ofstream carsFile(outputFileName);

    for(int i = 0; i < carsPath.length() - 1; i++)
    {
        // Separating path's parts & finding its difficulty
        string pathPart;
        pathPart += carsPath[i];
        pathPart += carsPath[i + 1];
        int pathPartEmission = pathPartsDifficultyTable.find(pathPart)->second;
        int curEdge;
        
        // Using monitor and calculating edge emission
        for(int q = 0; q < edgesInOrder.size(); q++)
        {
            if(edgesInOrder[q] == pathPart)
            {
                curEdge = q;
                break;
            }
        }

        monitorsReturnType carInEdgeInfo;
        carInEdgeInfo = allMonitors[curEdge]->carsPathPartEmissionCalc(carP, pathPartEmission, totalEmission);

        // Writing each line of the output file
        carsFile << pathPart[0] << ", " << carInEdgeInfo.entranceTime.count() << ", "
                 << pathPart[1] << ", " << carInEdgeInfo.exitTime.count() << ", "
                 << carInEdgeInfo.emission << ", " << totalEmission << endl;
        
    }

    carsFile.close();
    
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        cout << "Invalid input!\n";
        return 0;
    }

    string inputFileName = argv[1];
    ifstream inputFile;
	inputFile.open(inputFileName.c_str());   
    string line, part, key, value;
    bool hashtagSeen = false, gotKey = false, gotCarNumValue = false;
    map<string, int> pathPartsDifficultyTable;
    map<string, int> pathsStartingNodeCarNumTable;
    vector<string> edgesInOrder;
    vector<string> pathsInOrder;

    // Getting input from file
    while(getline(inputFile, line))
    {       
        if(line[0] != '#' && hashtagSeen == false)
        {
            gotKey = false;
            for(int i = 0; i < line.length(); i++)
            {
                if(line[i] != '-' && line[i] != ',' && line[i] != ' ' && line[i] != '\n' && line[i] != '\r')
                    part += line[i];
                if(part.length() == 2 && gotKey == false)
                {
                    key = part;
                    part = "";
                    gotKey = true;
                    edgesInOrder.push_back(key);
                }
            }
            value = part;
            part = "";
        }
        else if(line[0] != '#' && hashtagSeen == true)
        {
            for(int i = 0; i < line.length(); i++)
            {
                if(line[i] != '-' && line[i] != ',' && line[i] != ' ' && line[i] != '\n' && line[i] != '\r')
                    part += line[i];
            }
            if(gotKey == false)
            {
                key = part;
                part = "";
                gotKey = true;
                gotCarNumValue = false;
                pathsInOrder.push_back(key);
            }
            else
            {
                value = part; 
                part = "";
                gotKey = false;
                gotCarNumValue = true;
            }  
        }
        else
        {
            hashtagSeen = true;
            gotKey = false;
            continue;
        }

        if(hashtagSeen == false)
        {
            pathPartsDifficultyTable[key] = atoi(value.c_str());
            key = "";
            value = "";
        }
        else if(gotCarNumValue == true)
        {
            pathsStartingNodeCarNumTable[key] = atoi(value.c_str());
            key = "";
            value = "";
        }

    } 

    vector<thread> cars;
    int carP, totalEmission = 0;
    srand(time(NULL));

    // Making monitors for edges
    for(int i = 0; i < edgesInOrder.size(); i++)
    {
        monitor* edgeMonitor;
        edgeMonitor = new monitor();
        allMonitors.push_back(edgeMonitor);
    }

    // Creating threads (cars)
    for(auto elem : pathsStartingNodeCarNumTable)
    {
        for(int i = 0; i < elem.second; i++)
        {
            carP = (rand() % 10) + 1;
            int pathNumber;
            for(int j = 0; j < pathsInOrder.size(); j++)
            {
                if(elem.first == pathsInOrder[j])
                    pathNumber = j + 1;
            }
            cars.emplace_back(carWorker, pathPartsDifficultyTable, elem.first, carP, ref(totalEmission), pathNumber, i + 1, edgesInOrder);
        }
    }

    for(auto& t: cars)
    {
        t.join();
    }


}